EVENT POLL
==========

https://bitbucket.org/i-g-chernyakov/event-poll.git

:demo: http://s160708.cloud.flynet.pro/


About
-----

``EVENT POLL`` is a Django web-channels project. It provides live event pollings.


Authors
-------

* Igor Chernyakov <i.g.chernyakov@yandex.ru>