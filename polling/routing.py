from channels import route_class

from polling.consumers import PollingDemultiplexer


polling_routing = [
    route_class(PollingDemultiplexer, path='^/websockets/?$'),
]
