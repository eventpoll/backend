
from rest_framework import serializers

from .models import Polling, Event, EventName, Vote, Result, Participant


class PollingSerializer(serializers.ModelSerializer):
    user = serializers.ReadOnlyField(source='user.username')

    class Meta:
        model = Polling
        fields = ('id', 'user', 'name', 'description', 'first_phase_duration', 'second_phase_duration',
                  'original_hour', 'original_timezone',
                  'active', 'state', 'start_time', 'first_phase_finish_time', 'finish_time', 'terminated_at')
        read_only_fields = ('id', 'active', 'state', 'start_time', 'first_phase_finish_time', 'finish_time',
                            'terminated_at')


class EventNameSerializer(serializers.ModelSerializer):
    class Meta:
        model = EventName
        fields = ('id', 'name',)


class EventSerializer(serializers.ModelSerializer):
    # has_user_voted = serializers.BooleanField()
    event_text = serializers.ReadOnlyField(source='event_name.name', read_only=True)
    user_voted = serializers.SerializerMethodField(read_only=True)

    def get_user_voted(self, obj):
        if self.context and 'request' in self.context:
            return obj.has_user_voted(self.context['request'].user)
        else:
            return False

    class Meta:
        model = Event
        fields = '__all__'
        read_only_fields = ('polling',)


class VoteSerializer(serializers.ModelSerializer):
    class Meta:
        model = Vote
        fields = ('event', 'user')
        read_only_fields = ('user',)


class ResultSerializer(serializers.ModelSerializer):
    class Meta:
        model = Result
        fields = ('polling', 'event')
        read_only_fields = ('polling', 'event')


class ParticipantSerializer(serializers.ModelSerializer):
    user = serializers.ReadOnlyField(source='user.username')
    reason_text = serializers.CharField(source='get_reason_display')

    class Meta:
        model = Participant
        fields = ('polling', 'user', 'reason', 'reason_text')
        read_only_fields = ('polling', 'user', 'reason', 'reason_text')
