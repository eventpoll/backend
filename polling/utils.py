from functools import wraps


def convert_validation_error_to_dict(e):
    return {'message': getattr(e, "message", ""),
            'code': getattr(e, "code", ""),
            'params': getattr(e, "params", ""),
            'error_list': getattr(e, "error_list", "")}


class Singleton(type):
    """
    Define an Instance operation that lets clients access its unique
    instance.
    """

    def __init__(cls, name, bases, attrs, **kwargs):
        super().__init__(name, bases, attrs)
        cls._instance = None

    def __call__(cls, *args, **kwargs):
        if cls._instance is None:
            cls._instance = super().__call__(*args, **kwargs)
        return cls._instance


class SendSignalOnCreate(object):
    """
    If selfwrapped.pk is None wrapper sends the signal with a saved object.

    """
    def __init__(self, signal):
        self.signal = signal
        self.is_created = False

    def __call__(self, func):
        @wraps(func)
        def wrapper(selfwrapped, *args, **kwargs):
            if hasattr(selfwrapped, 'pk'):
                self.is_created = not selfwrapped.pk

            func(selfwrapped, *args, **kwargs)
            if self.is_created:
                self.signal.send(sender=selfwrapped.__class__, instance=selfwrapped)

        return wrapper
