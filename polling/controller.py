"""
Application logic controller.


"""

from channels.generic.websockets import WebsocketMultiplexer
from django.db.models import signals
from django.dispatch import receiver

from .utils import Singleton
from .settings import POLLING_CHANNEL_GROUP, POLLING_STREAM
from .tasks import set_finish_first_phase, set_finish_polling
from .models import Polling, Event, EventName, Vote, Participant
from .serializers import PollingSerializer, EventSerializer, EventNameSerializer, ParticipantSerializer


class Controller(metaclass=Singleton):
    """
    Application logic controller.

    """
    polling_channel_group = POLLING_CHANNEL_GROUP
    polling_sream = POLLING_STREAM

    @classmethod
    def user_channel_group(cls, user):
        """
        Returns name of channel group for user

        We use user.id to identify user channel.

        :param user: object User
        :return: str
        """

        if hasattr(user, 'id'):
            return "user_{}.channel.group".format(user.id)
        else:
            return "user_{}.channel.group".format(repr(user))

    def websocket_receive(self, user, content, **kwargs):
        pass

    @classmethod
    def send_message_to_all(cls, payload):
        WebsocketMultiplexer.group_send(name=POLLING_CHANNEL_GROUP, stream=POLLING_STREAM, payload=payload)

    @classmethod
    def send_message_to_user(cls, user, message):
        pass

    @classmethod
    def _send_action(cls, action, serializer, instance, extra_query_set=None):
        """ Send to channels processed actions

        :param action:
        :param serializer:
        :param instance:
        :param extra_query_set:
        :return:
        """
        payload = {'action': action,
                   'data': serializer(instance).data}
        if extra_query_set:
            payload['extra'] = serializer(extra_query_set, many=True).data

        Controller.send_message_to_all(payload)

    def send_start_polling(self, instance, **kwargs):
        self._send_action('start_polling', PollingSerializer, instance)

    def send_change_polling(self, instance, **kwargs):
        self._send_action('change_polling', PollingSerializer, instance)

    def send_add_event(self, instance, **kwargs):
        self._send_action('add_event', EventSerializer, instance, instance.polling.events)

    def send_change_event(self, instance, **kwargs):
        self._send_action('change_event', EventSerializer, instance, instance.polling.events)

    def send_remove_event(self, instance, **kwargs):
        self._send_action('remove_event', EventSerializer, instance, instance.polling.events)

    def send_add_event_name(self, instance, **kwargs):
        self._send_action('add_event_name', EventNameSerializer, instance, EventName.objects.all())

    def send_remove_event_name(self, instance, **kwargs):
        self._send_action('remove_event_name', EventNameSerializer, instance, EventName.objects.all())

    def send_add_participant(self, instance, **kwargs):
        self._send_action('add_participant', ParticipantSerializer, instance, instance.polling.participants)

    def send_remove_participant(self, instance, **kwargs):
        self._send_action('remove_participant', ParticipantSerializer, instance, instance.polling.participants)

    def send_voted_users_counter(self, voted_users_counter, **kwargs):
        payload = {'action': 'change_voted_users',
                   'data': {'counter': voted_users_counter}}
        self.send_message_to_all(payload)


polling_controller = Controller()


@receiver(signals.post_save, sender=Polling)
def polling_post_save(sender, instance, created, raw, using, update_fields, **kwargs):
    # The polling is just started. Put celery tasks
    if created:
        polling_controller.send_change_polling(instance)
        # set task to finish the first phase
        set_finish_first_phase.apply_async(
            (
                instance.pk,
                instance.first_phase_finish_time,
                polling_controller.polling_channel_group,
                polling_controller.polling_sream,
                {'action': 'finish_first_phase',
                 'data': PollingSerializer(instance).data}
            ),
            eta=instance.first_phase_finish_time
        )
        # set task to finish the polling
        set_finish_polling.apply_async(
            (
                instance.pk,
                instance.finish_time,
                polling_controller.polling_channel_group,
                polling_controller.polling_sream,
                {'action': 'finish_polling',
                 'data': PollingSerializer(instance).data}
            ),
            eta=instance.finish_time
        )
    else:
        polling_controller.send_change_polling(instance)


@receiver(signals.post_save, sender=Event)
def event_post_save(sender, instance, created, raw, using, update_fields, **kwargs):
    if created:
        polling_controller.send_add_event(instance)
    else:
        polling_controller.send_change_event(instance)


@receiver(signals.post_delete, sender=Event)
def event_post_delete(sender, instance, using, **kwargs):
    polling_controller.send_remove_event(instance)


@receiver(signals.post_save, sender=EventName)
def event_name_post_save(sender, instance, created, raw, using, update_fields, **kwargs):
    if created:
        polling_controller.send_add_event_name(instance)


@receiver(signals.post_delete, sender=EventName)
def event_name_post_delete(sender, instance, using, **kwargs):
    polling_controller.send_remove_event_name(instance)


@receiver(signals.post_save, sender=Participant)
def participant_post_save(sender, instance, created, raw, using, update_fields, **kwargs):
    if created:
        polling_controller.send_add_participant(instance)


@receiver(signals.post_delete, sender=Participant)
def participant_post_delete(sender, instance, using, **kwargs):
    polling_controller.send_remove_participant(instance)


@receiver(signals.post_save, sender=Vote)
def participant_post_save(sender, instance, created, raw, using, update_fields, **kwargs):
    if created:
        polling_controller.send_voted_users_counter(instance.event.polling.voted_users_counter)


@receiver(signals.post_delete, sender=Vote)
def participant_post_delete(sender, instance, using, **kwargs):
    polling_controller.send_voted_users_counter(instance.event.polling.voted_users_counter)
