from django.core.exceptions import ValidationError
from django.utils.translation import ugettext_lazy as _


class ExtraValidationError(ValidationError):
    message = ""
    code = ""
    params = ""

    def __init__(self):
        super(ExtraValidationError, self).__init__(
            message=self.message,
            code=self.code,
            params=self.params)


class PollingOnlyOneActiveValidationError(ExtraValidationError):
    message = _('There is already the active polling!')
    code = 'theonly'


class PollingIsNotActiveValidationError(ExtraValidationError):
    message = _('The polling is not active!')
    code = 'notactive'


class PollingCanNotChangeStateValidationError(ExtraValidationError):
    message = _('The polling state can not be changed!')
    code = 'notchanged'


class TaskError(Exception):
    pass
