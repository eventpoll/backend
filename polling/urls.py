from django.conf.urls import url, include
from rest_framework import routers

from .views import IndexView
from .api import PollingViewSet, ActivePollingManageSet, EventNameViewSet, EventViewSet
from .api import ResulViewSet, ParticipantViewSet


app_name = 'polling'

router = routers.SimpleRouter()
router.register(r'polling', PollingViewSet, base_name='polling')
router.register(r'active-polling', ActivePollingManageSet, base_name='active-polling')
router.register(r'event-name', EventNameViewSet, base_name='event-name')
router.register(r'polling/(?P<polling_id>[^/.]+)/participants', ParticipantViewSet, base_name='participants')
router.register(r'polling/(?P<polling_id>[^/.]+)/events', EventViewSet, base_name='events')
router.register(r'polling/(?P<polling_id>[^/.]+)/results', ResulViewSet, base_name='results')


urlpatterns = [
    url(r'^$', IndexView.as_view(), name='index'),
    url(r'^api/', include(router.urls, namespace='api')),
]
