import django.dispatch


#Polling signals
def newSignal():
    return django.dispatch.Signal(providing_args=['instance'])


sig_polling_first_phase_finished = newSignal()
sig_polling_finished = newSignal()
