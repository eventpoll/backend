import logging

from datetime import timedelta
from functools import wraps

import pytz
from django.core.exceptions import ValidationError
from django.conf import settings
from django.db import models
from django.db.models import Count, Max
from django.utils.translation import ugettext_lazy as _
from django.utils import timezone
from django_extensions.db.models import TimeStampedModel
from django.core.validators import MaxValueValidator, MinValueValidator
from django.db.utils import IntegrityError
from django.contrib.auth import get_user_model

from .exceptions import PollingOnlyOneActiveValidationError, PollingIsNotActiveValidationError
from .exceptions import PollingCanNotChangeStateValidationError


log = logging.getLogger(__name__)


USER_MODEL = getattr(settings, 'AUTH_USER_MODEL')


def must_be_active(func):
    @wraps(func)
    def wrapper(self):
        if not self.active:
            raise PollingIsNotActiveValidationError()
        func(self)
    return wrapper


def get_timezone(value):
    try:
        return pytz.timezone(value)
    except pytz.UnknownTimeZoneError as e:
        raise e


class PollingManager(models.Manager):
    def get_active_polling(self):
        try:
            p = self.get(actual=0)
            p.check_state()
            if p.active:
                return p
            else:
                return None
        except Polling.DoesNotExist:
            return None


class Polling(TimeStampedModel):
    """

    """
    FIRST_PHASE = "FIRST"
    SECOND_PHASE = "SECOND"
    FINISHED = "FINISHED"
    TERMINATED = "TERMINATED"

    POLLING_STATE_CHOICES = (
        (FIRST_PHASE, _('The polling is in the first phase.')),
        (SECOND_PHASE, _('The polling is in the second phase.')),
        (FINISHED, _('The polling is finished.')),
        (TERMINATED, _('The polling was terminated.')),
    )

    user = models.ForeignKey(USER_MODEL,
                             related_name='pollings',
                             verbose_name=_('Polling user'),
                             on_delete=models.PROTECT)
    name = models.CharField(verbose_name=_('Name'),
                            max_length=100,
                            default='',
                            blank=True)
    description = models.TextField(verbose_name=_('Description'),
                                   default='',
                                   blank=True)
    first_phase_duration = models.PositiveSmallIntegerField(verbose_name=_('First phase duration (min)'),
                                                            validators=[MinValueValidator(1)])
    second_phase_duration = models.PositiveSmallIntegerField(verbose_name=_('Second phase duration (min)'),
                                                             validators=[MinValueValidator(1)])
    start_time = models.DateTimeField(verbose_name=_('Starts at time'))
    first_phase_finish_time = models.DateTimeField(verbose_name=_('The first phase has to be finished at'))
    finish_time = models.DateTimeField(verbose_name=_('The polling has to be finished at'))
    terminated_at = models.DateTimeField(verbose_name=_('The time of the polling termination'),
                                         default=None,
                                         blank=True,
                                         null=True)
    original_hour = models.PositiveSmallIntegerField(verbose_name=_('Polling interval between original hour and 23'),
                                                     validators=[MaxValueValidator(23)],
                                                     default=0)
    original_timezone = models.CharField(verbose_name=_('Original timezone'),
                                         max_length=63,
                                         default='',
                                         blank=True)
    # The polling is active when polling.actual == 0 --
    actual = models.BigIntegerField(verbose_name=_('Actuality'),
                                    unique=True)
    state = models.CharField(verbose_name=_('The state of the polling'),
                             max_length=10,
                             choices=POLLING_STATE_CHOICES,
                             default=FIRST_PHASE)

    objects = PollingManager()

    def __str__(self):
        return "{} {}".format(self.id, self.name)

    @property
    def active(self):
        return self.actual == 0

    def get_voted_users(self):
        return get_user_model().objects.filter(votes__event__polling=self).distinct()

    def is_participant(self, user):
        return self.participants.filter(user=user).exists()

    @property
    def voted_users_counter(self):
        return get_user_model().objects.filter(votes__event__polling=self).distinct().count()

    def clean(self):
        if self.active:
            raise ValidationError(message=_('Can not modify the active polling'))

    def save(self, *args, **kwargs):
        if not self.pk:
            self._set_phase_times()
            self.state = self.FIRST_PHASE
            self.actual = 0

        try:
            super(Polling, self).save(*args, **kwargs)
        except IntegrityError as exc:
            raise PollingOnlyOneActiveValidationError()

    def _clear_results(self):
        for event in self.events.all():
            event.votes.all().delete()

        self.participants.all().delete()
        self.results.all().delete()

    def _set_phase_times(self):
        self.start_time = timezone.now()
        self.terminated_at = None
        self.first_phase_finish_time = self.start_time + timedelta(minutes=self.first_phase_duration)
        self.finish_time = self.first_phase_finish_time + timedelta(minutes=self.second_phase_duration)

    def _deactivate(self):
        self.actual = self.pk

    def _get_results(self):
        annotated_events = self.events.annotate(num_votes=Count('votes'))
        max_events = annotated_events.aggregate(Max('num_votes'))
        result_events = annotated_events.filter(num_votes=max_events['num_votes__max'])
        Result.objects.bulk_create([Result(polling=self, event=e) for e in result_events])

    def _get_list_of_participants_from_results(self):
        voters = self.user.__class__.objects.filter(votes__event__results__polling=self)
        for voter in set(voters):
            Participant.objects.create(polling=self, user=voter)

    @must_be_active
    def restart(self):
        self._clear_results()
        self._set_phase_times()
        self.state = self.FIRST_PHASE
        self.save()
        return self

    @must_be_active
    def terminate(self):
        self._deactivate()
        self.state = self.TERMINATED
        self.terminated_at = timezone.now()
        self.save()
        return self

    def check_state(self):
        """Return True if the polling state corresponds with start and finish time points.
        Otherwise correct state and return False

        :return: bool
        """

        if not self.active:
            if self.state == self.FINISHED or self.state == self.TERMINATED:
                return True
            else:
                # incorrect state. set terminated
                self.state = self.TERMINATED
                self.terminated_at = timezone.now()
                self.save()
                return False

        # The polling is active,
        # If all time points are not set then terminate
        if not self.start_time or not self.first_phase_finish_time or not self.finish_time:
            self.terminate()
            return False

        # Compare state, time points and current time
        now = timezone.now()
        if self.state == self.SECOND_PHASE:
            # normal second phase
            if self.first_phase_finish_time <= now < self.finish_time:
                return True

            # The time of polling is over but celery task is missed.
            if now > self.finish_time:
                self.finish_polling()
                return False

        if self.state == self.FIRST_PHASE:
            # normal first phase
            if self.start_time <= now < self.first_phase_finish_time:
                return True

            # The time of the first phase is over but celery task is missed
            if self.first_phase_finish_time < now <= self.finish_time:
                self.finish_first_phase()
                return False

        #  Other states of active polling are incorrect. So we terminate polling
        self.terminate()
        return False

    @must_be_active
    def finish_first_phase(self):
        if self.state == self.FIRST_PHASE:
            self._get_results()
            self._get_list_of_participants_from_results()
            self.state = self.SECOND_PHASE
            self.save()
            return self

        raise PollingCanNotChangeStateValidationError()

    @must_be_active
    def finish_polling(self):
        log.debug('Model finish polling.  self.state == {}'.format(self.state))
        if self.state == self.SECOND_PHASE:
            self._deactivate()
            self.state = self.FINISHED
            self.save()
            return self

        raise PollingCanNotChangeStateValidationError()


class Event(models.Model):
    polling = models.ForeignKey('Polling',
                                related_name='events',
                                verbose_name=_('Polling'))
    hour = models.PositiveSmallIntegerField(verbose_name=_('Hour'),
                                            validators=[MinValueValidator(0), MaxValueValidator(23)])
    event_name = models.ForeignKey('EventName',
                                   verbose_name=_('Event name'),
                                   on_delete=models.PROTECT)

    def clean(self):
        # hour should not be less than original_hour
        if not self.polling.active:
            raise ValidationError({_('You can add event to inactive polling!')})

        if self.polling.state != Polling.FIRST_PHASE:
            raise ValidationError({_('You can add event only when the first phase is active!')})

        if self.hour < self.polling.original_hour:
            raise ValidationError({'hour': _('An Event hour should be more or equal than the polling original hour')})

    def save(self, *args, **kwargs):
        self.full_clean()
        return super(Event, self).save(*args, **kwargs)

    def __str__(self):
        return "Polling {}; Event at {}, name {}".format(self.polling, self.hour, self.event_name)

    class Meta:
        unique_together = (("polling", "hour", "event_name"),)
        ordering = ['hour', 'event_name']

    def has_user_voted(self, user):
        return self.votes.filter(user=user).exists()


class EventName(models.Model):
    name = models.CharField(verbose_name=_('Event name'), max_length=100, unique=True)

    class Meta:
        ordering = ['name']

    def __str__(self):
        return self.name


class Vote(TimeStampedModel):
    event = models.ForeignKey('Event', related_name='votes', on_delete=models.PROTECT)
    user = models.ForeignKey(USER_MODEL, related_name='votes', on_delete=models.PROTECT)

    class Meta:
        unique_together = (("event", "user"),)

    def __str__(self):
        return "{}; {}; {}".format(self.event.polling, self.event, self.user)

    def clean(self):
        if self.event.polling.state != Polling.FIRST_PHASE:
            raise ValidationError(_('Polling is not in the first phase!'))

    def save(self, *args, **kwargs):
        self.full_clean()
        super(Vote, self).save(*args, **kwargs)


class Result(models.Model):
    polling = models.ForeignKey('Polling', related_name='results', on_delete=models.PROTECT)
    event = models.ForeignKey('Event', related_name='results', on_delete=models.PROTECT)

    class Meta:
        unique_together = (("polling", "event"),)


class ParticipantManager(models.Manager):
    def accept_polling(self, polling, user):
        if not polling.active:
            raise ValidationError(message=_('The polling is not active!'))

        if polling.state != Polling.SECOND_PHASE:
            raise ValidationError(message=_('The active polling is not at the second phase!'))

        try:
            return self.create(polling=polling, user=user, reason=Participant.ACCEPT)
        except IntegrityError as ie:
            raise ValidationError(message=_('The user is already the participant of the polling!'))


class Participant(models.Model):
    VOTE = "V"
    ACCEPT = "A"
    PARTICIPATION_REASON_CHOICES = (
        (VOTE, _('Vote for event')),
        (ACCEPT, _('Accept polling')),
    )

    objects = ParticipantManager()

    polling = models.ForeignKey('Polling', related_name='participants', on_delete=models.PROTECT)
    user = models.ForeignKey(USER_MODEL, related_name='participate_in', on_delete=models.PROTECT)
    reason = models.CharField(verbose_name=_('Reason of participation'),
                              max_length=1,
                              choices=PARTICIPATION_REASON_CHOICES,
                              default=VOTE)

    class Meta:
        unique_together = (("polling", "user"),)

    def save(self, *args, **kwargs):
        super(Participant, self).save(*args, **kwargs)
