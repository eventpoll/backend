import string
import random

import pytz
from django.contrib.auth.models import User
from django.utils import timezone
from django.utils.crypto import get_random_string
import factory

from polling.models import Polling, EventName


class EventNameFactory(factory.DjangoModelFactory):
    class Meta:
        model = EventName

    name = factory.Sequence(lambda n: 'event {0}'.format(n))


class UserFactory(factory.DjangoModelFactory):
    class Meta:
        model = User

    username = factory.Sequence(lambda n: 'user_{0}'.format(n))
    email = '{}@mail.com'.format(username)
    password = factory.PostGenerationMethodCall('set_password', 'password')


class PollingFactory(factory.DjangoModelFactory):
    class Meta:
        model = Polling

    user = factory.SubFactory(UserFactory)
    name = factory.Sequence(lambda n: 'polling {0}'.format(n))
    description = get_random_string(50, string.ascii_letters)
    first_phase_duration = random.randrange(1, 2)
    second_phase_duration = random.randrange(1, 2)
    original_timezone = random.choice(pytz.all_timezones)
    original_hour = timezone.now().astimezone(pytz.timezone(original_timezone)).hour
