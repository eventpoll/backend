import pytest

from django.core.exceptions import ValidationError
from django.db.utils import IntegrityError
from polling.exceptions import PollingOnlyOneActiveValidationError

from polling.tests import factories


@pytest.mark.django_db
class TestPolling(object):
    def test_create_first_polling(self):
        polling = factories.PollingFactory()
        assert polling.active

    def test_create_another_polling(self):
        factories.PollingFactory()
        with pytest.raises(PollingOnlyOneActiveValidationError):
            factories.PollingFactory()

    def test_terminate_polling(self):
        polling1 = factories.PollingFactory()
        polling1.terminate()
        polling2 = factories.PollingFactory()
        assert polling2.active
