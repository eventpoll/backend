import logging

from django.views import generic
from django.contrib.auth.mixins import LoginRequiredMixin


logger = logging.getLogger('eventpoll.polling.views')


class IndexView(LoginRequiredMixin, generic.TemplateView):
    """
    Single Page Application
    """
    template_name = 'polling/index.html'
