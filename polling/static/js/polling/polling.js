"use strict";

$(function () {

    axios.defaults.xsrfCookieName = 'csrftoken';
    axios.defaults.xsrfHeaderName = 'X-CSRFToken';

    const FIRST_PHASE = "FIRST";
    const SECOND_PHASE = "SECOND";
    const NOT_ACTIVE = "NOT_ACTIVE";
    const POLLING_STREAM = "polling.stream";
    const WEBSOCKETS_PATH = '/websockets/';

    function URLFabric(prefix) {
        this.prefix = prefix;
    }

    var generateSimpleURL = function (name) {
        return function (pk) {
            var path = this.prefix + name+ '/';
            if (pk === undefined) {
                return path;
            } else {
                return path + pk + '/';
            }
        }
    }
    URLFabric.prototype.Polling = generateSimpleURL('polling');
    URLFabric.prototype.Event = generateSimpleURL('event');
    URLFabric.prototype.EventName = generateSimpleURL('event-name');
    URLFabric.prototype.Vote = generateSimpleURL('vote');
    URLFabric.prototype.Participant = generateSimpleURL('participant');
    URLFabric.prototype.Restart = function () {
        return this.prefix + 'active-polling/' + 'restart/';
    }
    URLFabric.prototype.Terminate = function () {
        return this.prefix + 'active-polling/' + 'terminate/';
    }
    URLFabric.prototype.Results = function (polling_id) {
        return this.prefix + 'polling/' + polling_id + '/results/';
    }
    URLFabric.prototype.Events = function (polling_id) {
        return this.prefix + 'polling/' + polling_id + '/events/';
    }
    URLFabric.prototype.GetActive = function () {
        return this.prefix + 'polling/get_active_polling/';
    }

    var urls = new URLFabric('/api/');

    Vue.filter('twoDigits', (value) => {
        if ( value.toString().length <= 1 ) {
            return '0'+value.toString();
        }
        return value.toString();
    });

    Vue.component('new-polling', {
        template: '#new-polling',
        data: function() {
            return {
                name: "",
                description: "",
                firstPhaseDuration: 1,
                secondPhaseDuration: 1
            }
        },
        methods: {
            createNewPolling: function(){
                var p = {
                    name: this.name,
                    description: this.description,
                    first_phase_duration: this.firstPhaseDuration,
                    second_phase_duration: this.secondPhaseDuration,
                    original_hour: getOriginalHour(),
                    original_timezone: getOriginalTimeZone(),

                }
                this.$emit('newpolling', p);
                this.$emit('close');
            },
        },

    });

    Vue.component('polling-list', {
        template: '#polling-list',
        props: {
           model: Object,
        },
        data: function() {
           return {
               data: Object
           }
        },
        computed: {
           isFolder: function(){
             return this.model.folder;
           },
        },
        methods: {
           toggle: function(){

           },
        },
    });

    Vue.component('countdown-timer', {
        template: '#countdown-timer',
        props: {
            title: String,
            finish: Number,
        },
        data: function() {
           return {
               timerNow: Math.trunc((new Date()).getTime() / 1000),
               timerDiff: 0,
               timerTitle: this.title,
               timerFinish: this.finish,
               timerInterval: null,
           }
        },
        mounted() {
            this.timerInterval = setInterval(() => {
               this.timerNow =  Math.trunc((new Date()).getTime() / 1000);
            }, 1000);

        },
        watch: {
            timerNow: function(val) {
                this.timerDiff = this.timerFinish - this.timerNow;
                if (this.timerDiff <= 0) {
                    this.timerDiff = 0;
                    clearInterval(this.timerInterval);
                }
            }
        },
        computed: {
            seconds() {
                return Math.trunc(this.timerDiff) % 60;
            },
            minutes() {
                return Math.trunc(this.timerDiff / 60) % 60;
            },
            hours() {
                return Math.trunc(this.timerDiff / 60 / 60) % 24;
            },
        },
        methods: {
        },
    });

    Vue.component('event-list', {
        template: '#event-list',
        props: {
           model: Object,
        },
        mounted: function(){
            var vm = this;
            $(this.$el)
                .select2({data: this.options})

        },
        data: function() {
           return {
               data: Object
           }
        },
        computed: {
           isFolder: function(){
             return this.model.folder;
           },
        },
        methods: {
           toggle: function(){

           },
        },
    });

    Vue.component('vote-chart', {
        template: '#vote-chart',
        props: {
           model: Object,
        },
        data: function() {
           return {
               data: Object
           }
        },
        computed: {
           isFolder: function(){
             return this.model.folder;
           },
        },
        methods: {
           toggle: function(){

           },
        },
    });

    Vue.component('polling-result', {
        template: '#polling-result',
        props: {
           model: Object,
        },
        data: function() {
           return {
               data: Object
           }
        },
        computed: {
           isFolder: function(){
             return this.model.folder;
           },
        },
        methods: {
           toggle: function(){

           },
        },
    });

    Vue.component('restart-polling', {
        template: '#restart-polling',
        methods: {
            restart: function(){
                this.$emit('restart');
           },
        },
    });

    Vue.component('participants', {
        template: '#participants',
        props: {
           model: Object,
        },
        data: function() {
           return {
               data: Object
           }
        },
        computed: {
           isFolder: function(){
             return this.model.folder;
           },
        },
        methods: {
           toggle: function(){

           },
        },
    });

    var vm = new Vue({
        el: '#eventpoll',
        data: {
            activePolling: null,
            pollingList: null,
            showNewPolling: false,
            firstPhaseTimer: {
                title: "Осталось до завершения первой фазы ",
                finish: 0,
            },
            finishTimer: {
                title: "Осталось до завершения голосования",
                finish: 0,
            },
            votedUsersCounter: 0,
        },
        beforeMount() {
            this.getActivePolling();
        },
        methods: {
            createPolling: function (p) {
                var thisvm = this;
                axios.post(urls.Polling(), p)
                    .then(function (responce) {
                        thisvm.activePolling = responce.data;
                        alert('Голосование открыто!');
                    })
                    .catch(function (error) {
                        alert('Ошибка при создании голосования: ' +
                            error.response.data.message);
                    });
            },
            addNewEvent: function () {
                this.events.push({
                    id: this.nextEventId++,
                    hour: this.eventHour,
                    name: this.newEventTitle
                });
                this.newEventTitle = "";
            },
            getPollingList: function () {
              var thisvm = this;
              axios.get(urls.Polling())
                    .then(function (response) {
                        thisvm.pollingList = response.data;
                    })
                    .catch(function (error) {
                        console.log(error);
                    });
            },
            getActivePolling: function(){
                var thisvm = this;
                axios.get(urls.GetActive())
                    .then(function (response) {
                        thisvm.activePolling = response.data;
                    })
                    .catch(function (error) {
                        if (error.response !== undefined) {
                            if (error.response.status == 404) {
                                thisvm.setInactive();
                            } else {
                                console.log(error);
                            }
                        } else {
                            console.log(error);
                        }
                    });
            },
            setInactive: function (){
                this.currentState = NOT_ACTIVE;
                this.activePolling = null;
                this.firstPhaseTimer.finish = 0;
                this.finishTimer.finish = 0;
            },
            setFirstPhase: function(polling) {
                this.activePolling = polling;
                this.currentState = FIRST_PHASE;
                this.firstPhaseTimer.finish =
                    Math.trunc(Date.parse(this.activePolling.first_phase_finish_time)/1000);
                this.finishTimer.finish =
                    Math.trunc(Date.parse(this.activePolling.finish_time)/1000);
            },
            startSecondPhase: function(polling) {

            },
            restartPolling: function () {
                var thisvm = this;
                axios.get(urls.Restart())
                    .then(function (response) {
                        thisvm.getActivePolling();
                    })
                    .catch(function (error) {
                        console.log(error);
                    });
            },
            terminatePolling: function () {
                var thisvm = this;
                axios.get(urls.Terminate())
                    .then(function (response) {
                        thisvm.getActivePolling();
                    })
                    .catch(function (error) {
                        console.log(error);
                    });
            },
            finishPolling: function (polling) {

            },
            changePolling: function (polling) {

            },

            setState: function (polling){
            },
            getPollingList: function (){

            },
        },
        watch: {
            activePolling: function(newVal, oldVal) {
                if (this.activePolling) {
                    this.firstPhaseTimer.finish =
                        Math.trunc(Date.parse(this.activePolling.first_phase_finish_time)/1000);
                    this.finishTimer.finish =
                        Math.trunc(Date.parse(this.activePolling.finish_time)/1000);
                } else {
                    this.firstPhaseTimer.finish = 0;
                    this.finishTimer.finish = 0;
                }
            },
        },
        computed: {
            currentHour: function() {
              return new Date().getHours();
            },
            isFirstPhase: function() {
                return (this.activePolling.state === FIRST_PHASE);
            },
            startTime: function() {
                if (!this.activePolling) {
                    return '';
                }
                var date = new Date(this.activePolling.start_time);
                return date.toLocaleString();
            }
        },
    });

    function ActionFactory() {
        this.getAction = function (action) {
            return this.actions[action];
        }
    }

    ActionFactory.prototype.actions = {};
    var StartPolling = function (data) {
            var polling = data;
            alert('Голосование открыто!');
        },
        FinishFirstPhase = function (data) {
            alert("Первая фаза голосования завершена!");
            vm.activePolling = data;
        },

        FinishPolling = function (data) {
        },
        ChangePolling = function (data) {
            console.log(data);

        },
        AddEvent = function (data) {

        },
        RemoveEvent = function (data) {

        },
        AddEventName = function (data) {

        },
        RemoveEventName = function (data) {

        },
        AddParticipant = function (data) {

        },
        RemoveParticipant = function (data) {

        },
        ChangeVotedUsers = function (data) {

        };
    ActionFactory.prototype.actions['start_polling'] = StartPolling;
    ActionFactory.prototype.actions['finish_first_phase'] = FinishFirstPhase;
    ActionFactory.prototype.actions['finish_polling'] = FinishPolling;
    ActionFactory.prototype.actions['change_polling'] = ChangePolling;
    ActionFactory.prototype.actions['add_event'] = AddEvent;
    ActionFactory.prototype.actions['remove_event'] = RemoveEvent;
    ActionFactory.prototype.actions['add_event_name'] = AddEventName;
    ActionFactory.prototype.actions['remove_event_name'] = RemoveEventName;
    ActionFactory.prototype.actions['add_participant'] = AddParticipant;
    ActionFactory.prototype.actions['remove_participant'] = RemoveParticipant;
    ActionFactory.prototype.actions['change_voted_users'] = ChangeVotedUsers;


    var actionFactory = new ActionFactory();

    var webSocketBridge = new channels.WebSocketBridge();
    webSocketBridge.connect(WEBSOCKETS_PATH);
    webSocketBridge.listen();

    webSocketBridge.demultiplex(POLLING_STREAM, function(action, stream) {
        actionFactory.getAction(action.action)(action.data);
    });

    function getOriginalHour() {
        var d = new Date();
        return d.getHours();
    };

    function getOriginalTimeZone() {
        return Intl.DateTimeFormat().resolvedOptions().timeZone;
    };


    // Helpful debugging
    webSocketBridge.socket.onopen = function () {
        console.log("Connected to socket");
    };
    webSocketBridge.socket.onclose = function () {
        console.log("Disconnected from socket");
    };
});