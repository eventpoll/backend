"""
Api for rest-framework
"""
from django.core.exceptions import ValidationError as DjangoValidationError
from django.http import Http404

from rest_framework.authentication import SessionAuthentication, BasicAuthentication
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response
from rest_framework import viewsets, generics, mixins, status
from rest_framework.decorators import detail_route, list_route
from rest_framework.exceptions import NotFound, PermissionDenied, ValidationError

from .permissions import IsOwner
from .models import Polling, Event, EventName, Result, Participant, Vote
from .serializers import PollingSerializer, EventNameSerializer, EventSerializer
from .serializers import VoteSerializer, ResultSerializer, ParticipantSerializer
from .utils import convert_validation_error_to_dict


class AuthenticationMixin(object):
    authentication_classes = (SessionAuthentication, BasicAuthentication)
    permission_classes = (IsAuthenticated, )


class IsOwnerMixin(AuthenticationMixin):
    permission_classes = (IsAuthenticated, IsOwner)


class PollingViewSet(AuthenticationMixin,
                     mixins.RetrieveModelMixin,
                     mixins.ListModelMixin,
                     mixins.CreateModelMixin,
                     viewsets.GenericViewSet):
    serializer_class = PollingSerializer

    def get_queryset(self):
        return Polling.objects.all()

    def perform_create(self, serializer):
        try:
            serializer.save(user=self.request.user)
        except DjangoValidationError as e:
            raise ValidationError(detail=convert_validation_error_to_dict(e))

    @list_route()
    def get_active_polling(self, request):
        active_polling = Polling.objects.get_active_polling()
        if active_polling:
            serializer = PollingSerializer(active_polling)
            return Response(serializer.data)
        else:
            return Response({'message': 'no active'}, status=status.HTTP_404_NOT_FOUND)


class ActivePollingManageSet(IsOwnerMixin,
                             viewsets.GenericViewSet):
    """
    Api to restart and terminate the active polling

    """
    serializer_class = PollingSerializer

    def get_queryset(self):
        return Polling.objects.filter(actual=0)

    def get_object(self):
        obj = Polling.objects.get_active_polling()
        if not obj:
            raise NotFound

        self.check_object_permissions(self.request, obj)
        return obj

    @list_route(url_name='restart')
    def restart(self, request):
        try:
            self.get_object().restart()
        except DjangoValidationError as e:
            raise ValidationError(detail=convert_validation_error_to_dict(e))
        else:
            return Response({'code': 'ok', 'detail': 'The polling was restarted'})

    @list_route(url_name='terminate')
    def terminate(self, request):
        try:
            self.get_object().terminate()
        except DjangoValidationError as e:
            raise ValidationError(detail=convert_validation_error_to_dict(e))
        else:
            return Response({'code': 'ok', 'detail': 'The polling was terminated'})


class EventNameViewSet(AuthenticationMixin,
                       viewsets.ModelViewSet):
    serializer_class = EventNameSerializer

    def get_queryset(self):
        return EventName.objects.all()


class EventViewSet(AuthenticationMixin,
                   mixins.CreateModelMixin,
                   mixins.ListModelMixin,
                   mixins.RetrieveModelMixin,
                   mixins.DestroyModelMixin,
                   viewsets.GenericViewSet):
    serializer_class = EventSerializer

    def get_queryset(self):
        polling_id = self.kwargs['polling_id']
        return Event.objects.filter(polling_id=int(polling_id))

    def get_serializer_context(self):
        return {'request': self.request}

    def perform_create(self, serializer):
        """
        Create event

        :param serializer:
        :return:
        """
        p = Polling.objects.get(id=self.kwargs['polling_id'])
        serializer.save(polling=p)

    @detail_route(methods=['get'], url_path='vote')
    def vote(self, request, polling_id=None, pk=None):
        """
        Vote for event

        """
        try:
            vote = Vote.objects.create(event_id=pk, user=request.user)
        except DjangoValidationError as e:
            raise ValidationError(detail=convert_validation_error_to_dict(e))
        else:
            return Response(VoteSerializer(vote).data)


class ParticipantViewSet(AuthenticationMixin,
                         mixins.ListModelMixin,
                         viewsets.GenericViewSet):
    serializer_class = ParticipantSerializer

    def get_queryset(self):
        polling_id = self.kwargs['polling_id']
        return Participant.objects.filter(polling_id=int(polling_id))

    def get_serializer_context(self):
        return {'request': self.request}

    @list_route(url_path='participate')
    def participate(self, request, polling_id=None):
        user = self.request.user
        try:
            polling = Polling.objects.get(pk=polling_id)
            p = Participant.objects.accept_polling(polling, user)
        except DjangoValidationError as e:
            raise ValidationError(detail=convert_validation_error_to_dict(e))
        else:
            return Response({'code': 'ok', 'detail': ParticipantSerializer(p).data})


class ResulViewSet(AuthenticationMixin,
                   mixins.ListModelMixin,
                   viewsets.GenericViewSet):
    serializer_class = ResultSerializer

    def get_queryset(self):
        polling_id = self.kwargs['polling_id']
        return Result.objects.filter(polling_id=int(polling_id))
