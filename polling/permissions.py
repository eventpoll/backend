from rest_framework import permissions


class IsOwner(permissions.BasePermission):
    def has_object_permission(self, request, view, obj=None):
        if obj:
            return obj.user == request.user
        else:
            return True
