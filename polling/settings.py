from django.conf import settings


# name of websockets stream
POLLING_STREAM = getattr(settings, 'POLLING_STREAM', 'polling.stream')

# name of global channel group
POLLING_CHANNEL_GROUP = getattr(settings, 'POLLING_CHANNEL_GROUP', 'polling.channel.group')


