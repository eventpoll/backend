from channels.generic.websockets import WebsocketDemultiplexer, JsonWebsocketConsumer
from channels import Group

from .controller import polling_controller


class PollingConsumer(JsonWebsocketConsumer):
    http_user_and_session = True
    strict_ordering = True

    def connection_groups(self, multiplexer):
        """
         Use one group for all channels
         Username groups are used to keep user channels
        """
        return [polling_controller.polling_channel_group]

    def connect(self, message, **kwargs):
        Group(polling_controller.user_channel_group(message.user)).add(message.reply_channel)

    def receive(self, content, **kwargs):
        polling_controller.websocket_receive(self.message.user, content, **kwargs)

    def disconnect(self, message, **kwargs):
        Group(polling_controller.user_channel_group(message.user)).discard(message.reply_channel)


class PollingDemultiplexer(WebsocketDemultiplexer):

    http_user_and_session = True
    strict_ordering = True

    consumers = {
        polling_controller.polling_sream: PollingConsumer,
    }

    def connection_groups(self):
        return [polling_controller.polling_channel_group]
