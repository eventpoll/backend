import logging
import json

from django.utils import timezone
from channels.generic.websockets import WebsocketMultiplexer
from dateutil import parser

from eventpoll.celery import app
from polling.models import Polling
from polling.exceptions import TaskError


log = logging.getLogger(__name__)


def _to_datetime(str):
    return parser.parse(str).astimezone(tz=timezone.get_current_timezone())


@app.task
def set_finish_first_phase(pk, first_phase_finish_time, channel, stream, payload):
    try:
        p = Polling.objects.get(pk=pk)
    except Polling.DoesNotExist as err:
        raise TaskError(repr(err))

    if p.first_phase_finish_time == _to_datetime(first_phase_finish_time):
        p.finish_first_phase()
        WebsocketMultiplexer.group_send(name=channel, stream=stream, payload=payload)
        return 0
    else:
        raise TaskError('Polling first_phase_finish_time was changed')


@app.task
def set_finish_polling(pk, finish_time, channel, stream, payload):
    try:
        p = Polling.objects.get(pk=pk)
    except Polling.DoesNotExist as err:
        raise TaskError(repr(err))

    if p.finish_time == _to_datetime(finish_time):
        p.finish_polling()
        WebsocketMultiplexer.group_send(name=channel, stream=stream, payload=payload)
        return 0
    else:
        raise TaskError('Polling finish_time was changed')
